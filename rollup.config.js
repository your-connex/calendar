import svelte from "rollup-plugin-svelte";
import sass from "rollup-plugin-scss";
import { writeFileSync } from "fs";

const production = !process.env.ROLLUP_WATCH;

export default [
    {
        input: "packages/core/src/index.js",
        output: {
            format: "es",
            file: "packages/core/index.js",
        },
        external: ["svelte", "svelte/internal", "svelte/store"],
        plugins: [
            svelte({
                compilerOptions: {
                    // enable run-time checks when not in production
                    dev: !production,
                    discloseVersion: false,
                    css: false,
                },
            }),
            // we'll extract any component CSS out into
            // a separate file - better for performance
            sass({
                output: (styles, styleNodes) => {
                    writeFileSync("packages/core/index.css", styles);
                },
            }),
        ],
    },
    {
        input: "packages/interaction/src/index.js",
        output: {
            format: "es",
            file: "packages/interaction/index.js",
        },
        external: [
            "@your-connex/calendar-core",
            "svelte",
            "svelte/internal",
            "svelte/store",
        ],
        plugins: [
            svelte({
                compilerOptions: {
                    dev: !production,
                    discloseVersion: false,
                    css: false,
                },
            }),
        ],
    },
    {
        input: "packages/day-grid/src/index.js",
        output: {
            format: "es",
            file: "packages/day-grid/index.js",
        },
        external: [
            "@your-connex/calendar-core",
            "svelte",
            "svelte/internal",
            "svelte/store",
        ],
        plugins: [
            svelte({
                compilerOptions: {
                    dev: !production,
                    discloseVersion: false,
                    css: false,
                },
            }),
        ],
    },
    {
        input: "packages/list/src/index.js",
        output: {
            format: "es",
            file: "packages/list/index.js",
        },
        external: [
            "@your-connex/calendar-core",
            "svelte",
            "svelte/internal",
            "svelte/store",
        ],
        plugins: [
            svelte({
                compilerOptions: {
                    dev: !production,
                    discloseVersion: false,
                    css: false,
                },
            }),
        ],
    },
    {
        input: "packages/time-grid/src/index.js",
        output: {
            format: "es",
            file: "packages/time-grid/index.js",
        },
        external: [
            "@your-connex/calendar-core",
            "svelte",
            "svelte/internal",
            "svelte/store",
        ],
        plugins: [
            svelte({
                compilerOptions: {
                    dev: !production,
                    discloseVersion: false,
                    css: false,
                },
            }),
        ],
    },
    {
        input: "packages/resource-time-grid/src/index.js",
        output: {
            format: "es",
            file: "packages/resource-time-grid/index.js",
        },
        external: [
            "@your-connex/calendar-core",
            "@your-connex/calendar-time-grid",
            "svelte",
            "svelte/internal",
            "svelte/store",
        ],
        plugins: [
            svelte({
                compilerOptions: {
                    dev: !production,
                    discloseVersion: false,
                    css: false,
                },
            }),
        ],
    },
];

function serve() {
    let started = false;

    return {
        writeBundle() {
            if (!started) {
                started = true;

                require("child_process").spawn(
                    "npm",
                    ["run", "start", "--", "--dev"],
                    {
                        stdio: ["ignore", "inherit", "inherit"],
                        shell: true,
                    }
                );
            }
        },
    };
}
